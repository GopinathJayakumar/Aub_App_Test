package stepDef;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.android.AndroidDriver;

public class Sms {
	public AndroidDriver<WebElement> driver;
	public int i=1;
	
	@And("launch App (.*),(.*) device")
	public void launchApp(String deviceName,String appPackage) throws MalformedURLException {
		DesiredCapabilities dc = new DesiredCapabilities();		
		dc.setCapability("appPackage", "com.android.mms");
		dc.setCapability("appActivity", "com.android.mms.ui.ConversationComposer");
		dc.setCapability("deviceName", deviceName);
		
		driver = new AndroidDriver<WebElement>(new URL("http://0.0.0.0:4723/wd/hub"), dc);
	}
	
	@And("fab_img click")
	public void clickButton() {
		driver.findElementById("fab_img").click();
	}
	
	@And("Print")
	public void print() {
		System.out.println("Iam in background");
	}
	@Given("fab_img click1")
	public void clickButton1() {
		driver.findElementById("fab_img").click();
	}
	@Then("take snap")
	public void takeSnap() {
		try {
			FileUtils.copyFile(driver.getScreenshotAs(OutputType.FILE), 
					new File("./snaps/imp"+i+".png"));
		} catch (WebDriverException | IOException e) {
			
		}
		i++;
	}
	
	@Before
	public void before() {
		System.out.println("Before method");
	}
	
	@After
	public void after() {
		System.out.println("After method");
	}

}
