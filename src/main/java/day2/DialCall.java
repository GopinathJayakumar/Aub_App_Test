package day2;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.android.AndroidDriver;

public class DialCall {

	public static void main(String[] args) throws MalformedURLException, InterruptedException {

		DesiredCapabilities dc = new DesiredCapabilities();
		dc.setCapability("deviceName", "LG V30 plus");
		dc.setCapability("platform", "Android");
		dc.setCapability("version", "Android");
		dc.setCapability("appPackage", "com.android.contacts");
		dc.setCapability("appActivity", "com.android.contacts.activities.DialtactsActivity");


		AndroidDriver<WebElement> driver = new AndroidDriver<WebElement>
		(new URL("http://0.0.0.0:4723/wd/hub"), dc);

		driver.findElementByAndroidUIAutomator("new UiSelector().text(\"9\")").click();
		driver.findElementByAndroidUIAutomator("new UiSelector().text(\"9\")").click();
		driver.findElementByAndroidUIAutomator("new UiSelector().text(\"4\")").click();
		driver.findElementByAndroidUIAutomator("new UiSelector().text(\"0\")").click();
		driver.findElementByAndroidUIAutomator("new UiSelector().text(\"3\")").click();
		driver.findElementByAndroidUIAutomator("new UiSelector().text(\"4\")").click();
		driver.findElementByAndroidUIAutomator("new UiSelector().text(\"5\")").click();
		driver.findElementByAndroidUIAutomator("new UiSelector().text(\"8\")").click();
		driver.findElementByAndroidUIAutomator("new UiSelector().text(\"2\")").click();
		driver.findElementByAndroidUIAutomator("new UiSelector().text(\"0\")").click();
		
		driver.findElementById("btnVoLTE").click();
		
		Thread.sleep(2000);
		
		String name = driver.findElementById("com.android.incallui:id/name").getText();
		System.out.println(name);
		
		//driver.findElementById("com.android.incallui:id/endButton").click();
	}
}
