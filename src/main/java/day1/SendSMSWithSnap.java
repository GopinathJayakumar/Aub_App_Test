package day1;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

import io.appium.java_client.android.AndroidDriver;

public class SendSMSWithSnap {
	
	public AndroidDriver<WebElement> driver;
	public int i=1;
	
	@Test
	public void main() throws MalformedURLException, InterruptedException {
			
		DesiredCapabilities dc = new DesiredCapabilities();		
		dc.setCapability("appPackage", "com.android.mms");
		dc.setCapability("appActivity", "com.android.mms.ui.ConversationList");
		dc.setCapability("deviceName", "LG");
		
		driver = new AndroidDriver<WebElement>(new URL("http://0.0.0.0:4723/wd/hub"), dc);		
		
		driver.findElementById("fab_img").click();
		takeSnap();
		
		driver.findElementByClassName("android.widget.ImageButton").click();
		
		driver.findElementById("android:id/search_src_text").sendKeys("M Lokesh");
		
		driver.findElementByAccessibilityId("M Lokesh").click();
		
		driver.findElementById("android:id/button1").click();
		takeSnap();
		
		driver.findElementById("com.android.contacts:id/btn_done").click();
		
		String text = driver.findElementByAccessibilityId("Enter message").getText();
		System.out.println(text);		
			
		driver.findElementByAccessibilityId("Enter message").sendKeys("hi Loosu");
		
		takeSnap();
		
		driver.findElementById("com.android.mms:id/send_button_text").click();
		
		//driver.hideKeyboard();
		
		List<WebElement> allTextE = driver.findElementsById("com.android.mms:id/text_view");
		System.out.println(allTextE.size());
		
		for (WebElement eachE : allTextE) {
			System.out.println(eachE.getText());
		}		
	}
	
	
	public void takeSnap() {
		try {
			FileUtils.copyFile(driver.getScreenshotAs(OutputType.FILE), new File("./snaps/imp"+i+".png"));
		} catch (WebDriverException | IOException e) {
			
		}
		i++;
	}
}
