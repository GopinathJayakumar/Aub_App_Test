package runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;


@CucumberOptions(features= {"src/main/java/features"},
					glue= {"stepDef"},dryRun=true,monochrome=true,
					strict=true,name="Send the SMS",tags="@Smoke")

@RunWith(Cucumber.class)

public class Runner {

}
