package testCases;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class TestFlipkart {
	
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
	
		RemoteWebDriver driver;
		
		DesiredCapabilities capabilities = new DesiredCapabilities();
//		capabilities.setCapability("browserName", "chrome");
		capabilities.setCapability("deviceName", "RQ30011RJZ");
		capabilities.setCapability("platformVersion", "6.0");
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("network", ".");
		capabilities.setCapability("full-reset", "true");
		capabilities.setCapability("appPackage", "com.flipkart.android");
		capabilities.setCapability("appActivity", "com.flipkart.android.SplashActivity");

		try
		{
			driver = new RemoteWebDriver(new URL("http://127.0.0.1:4725/wd/hub"), capabilities);
			driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
			Thread.sleep(5000);
			 
			//click the menu button
			List<WebElement> menuButton = driver.findElements(By.className("android.widget.ImageButton"));
			menuButton.get(0).click();
			
			//click My Account option
			driver.findElement(By.xpath("//android.widget.TextView[@text = 'My Account']")).click();
			
			//Enter Email ID
			WebElement usernameTextbox = driver.findElement(By.id("com.flipkart.android:id/mobileNo"));
			usernameTextbox.clear();
			usernameTextbox.sendKeys("gopekannan@gmail.com");
			
			//Enter Password
			driver.findElement(By.id("com.flipkart.android:id/et_password")).sendKeys("maruthi");
			
			//Click Sign-In
			driver.findElement(By.id("com.flipkart.android:id/btn_mlogin")).click();
			
		}
		catch(MalformedURLException e)
		{
			e.printStackTrace();
		}
		

	}

}
