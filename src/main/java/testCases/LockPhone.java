package testCases;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.android.AndroidDriver;

public class LockPhone {

	public static void main(String[] args) throws InterruptedException, MalformedURLException {
		DesiredCapabilities dc = new DesiredCapabilities();
		dc.setCapability("deviceName", "33000b83a318329f");
		dc.setCapability("appPackage", "com.testleaf.leaforg");
		dc.setCapability("appActivity", "com.testleaf.leaforg.MainActivity");
		AndroidDriver<WebElement> driver = new AndroidDriver<WebElement>(new URL ("http://0.0.0.0:4723/wd/hub"),dc);
		Thread.sleep(5000);
		driver.rotate(ScreenOrientation.LANDSCAPE);
		System.out.println(driver.getOrientation());
		
		driver.lockDevice();
		System.out.println("LOCKED");
		Thread.sleep(5000);
		driver.unlockDevice();
		System.out.println("UNLOCKED");
		/*
		if(driver.isLocked()){
			driver.lockDevice();
			System.out.println("LOCKED");
			driver.unlockDevice();
			System.out.println("UNLOCKED");
			
		}else{
			driver.unlockDevice();
			Thread.sleep(5000);
			driver.lockDevice();
		}*/
			
	}

}
